/*
  Задание:
  1. При помощи методов изложеных в arrays.js , переформатировать ITEA_COURSES в массив который содержит длину строк каждого из элементов.
  2. Самостоятельно изучить метод Array.sort. Отфильтровать массив ITEA_COURSES по алфавиту.
      + Бонусный бал. Вывести на страничку списком
  3. Реализация функции поиска по массиву ITEA_COURSES.
      + Бонусный бал. Вывести на страничку инпут и кнопку по которой будет срабатывать поиск.

*/

const ITEA_COURSES = ["Курс HTML & CSS", "JavaScript базовый курс", "JavaScript продвинутый курс", "JavaScript Professional", "Angular 2.4 (базовый)", "Angular 2.4 (продвинутый)", "React.js", "React Native", "Node.js", "Vue.js"];


// 1
const lengthArray = ITEA_COURSES.map(item => item.split('').length);
console.log(lengthArray);

// 2
const sortedArray = ITEA_COURSES.sort();
console.log(sortedArray);
const tree = document.createElement('ul');
sortedArray.forEach(el => {
    let li = document.createElement('li');
    li.innerHTML = `<li>${el}</li>`;
    tree.appendChild(li);
});
document.body.appendChild(tree);

// 3
let input = document.createElement('form');
input.innerHTML = `<input id='input' type='text'><button type='submit'>Find</button>`;
document.body.appendChild(input);
let filter = '';
let result;
input.addEventListener('submit', (e) => {
    // if(e.target.className === )
    filter = document.querySelector('#input').value;
  
    result = ITEA_COURSES.find(function(i){
        console.log('filter', filter, i)
        return i === filter
    });
    console.log(result);
e.preventDefault();
});

// input.addEventListener('input', () => {
//     // console.log(input.value);
    
//     console.log(result);

//     let list = document.createElement('ul');
//     result.forEach(el => {
//         let li = document.createElement('li');
//         li.innerHTML = `<li>${el}RESULT</li>`;
//         list.appendChild(li);
//     });
//     let resField = document.createElement('div');
//     resField.innerHTML = list;
// });

