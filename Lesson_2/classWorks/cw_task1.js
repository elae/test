/*

  Данные: http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
  Задача.

  1.  Получить данные и в виде простой таблички вывести список компаний. Для начала используем поля:
      Company | Balance | Registered | Показать адресс | Кол-во employers | показать сотрудников

  2.  Сделать сортировку таблицы по количеству сотрудников и балансу. Сортировка должна происходить по клику
      на заголовок столбца

  3.  По клику на показать адресс должна собиратся строка из полей адресса и показываться на экран.

  4.  По клику на показать сотрудников должна показываться другая табличка формата:

      <- Назад к списку компаний | *Название компании*
      Сотрудники:
      Name | Gender | Age | Contacts

  5.  В второй табличке долен быть реализован поиск сотрудников по их имени, а так же сортировка по
      полу и возрасту.

  Примечание: Весь код должен писатся с учетом синтаксиса и возмжность ES6.

*/
class Record {
	constructor(company, balance, registered, address, employers){
		this.company = company;
		this.balance = balance;
		this.registered = registered;
		this.address = address;
		this.employers = employers;
	}
}

class UI {
	addRow(input){
		let table = document.querySelector('.table');
		let row = document.createElement('tr');
		row.className = 'item';
		row.innerHTML = `
			<td>${input.company}</td>
			<td>${input.balance}</td>
			<td>${input.registered}</td>
			<td><button class='addr'>Show address</button></td>
			<td>${input.employers.length} employers</td>
			<td><button class='emp'>Show employers</button></td>
		`;
		table.appendChild(row);
	};	
	
	showAddress(target, result){
		target.innerHTML = `
			<ul>
				<li><b>city: </b>${result.city}</li>
				<li><b>country: </b>${result.country}</li>
				<li><b>house: </b>${result.house}</li>
				<li><b>state: </b>${result.state}</li>			
				<li><b>street: </b>${result.street}</li>
				<li><b>zip: </b>${result.zip}</li>
			</ul>
		`;
	}

	showEmployers(companyName, employers) {
		let oldTable = document.querySelector('.main');
		let empTable = document.createElement('table');
		empTable.className = "emp table table-bordered";		
		empTable.innerHTML = `
			<thead>
			<tr>
			<th><button class='back'>Back to company list</button></th>
			<th>${companyName}</th>
			</tr>
			<tr><th>Employers</th></tr>
			<tr>
			<th id='name-head'>Name</th>
			<th id='gender-head'>Gender</th>
			<th id='age-head'>Age</th>
			<th>Contacts</th>
			</tr>
			</thead>		
		`;
		document.body.replaceChild(empTable, oldTable);

		// Add employer row
		function addEmpRow() {
			employers.forEach(emp => {
			let row = document.createElement('tr');
			row.className = 'empRow';
			row.innerHTML = `
				<td>${emp.name}</td>
				<td>${emp.gender}</td>
				<td>${emp.age}</td>
				<td>Phone: ${emp.phones} <br> Email: ${emp.emails}</td>
			`;
			empTable.appendChild(row);
			})
		};
		addEmpRow();

		// Back to Main Table event
		document.querySelector('.back').addEventListener('click', () => {
			document.body.replaceChild(oldTable, empTable);
		});

		// Sort employers by Gender
		document.querySelector('#gender-head').addEventListener('click', () => {
			employers.sort((a, b) => {
				if(a.gender < b.gender) return -1;
				if(a.gender > b.gender) return 1;
				return 0;
			});
			while (empTable.contains(document.querySelector('.empRow'))) {
        empTable.removeChild(empTable.lastChild);
    	};
			addEmpRow();
		});

		// Sort employers by Age
		document.querySelector('#age-head').addEventListener('click', () => {
			employers.sort((a, b) => {
				if(a.age < b.age) return -1;
				if(a.age > b.age) return 1;
				return 0;
			});
			while (empTable.contains(document.querySelector('.empRow'))) {
        empTable.removeChild(empTable.lastChild);
    	};
			addEmpRow();
		});

		// Sort employers by Name
		document.querySelector('#name-head').addEventListener('click', () => {
			employers.sort((a, b) => {
				if(a.name < b.name) return -1;
				if(a.name > b.name) return 1;
				return 0;
			});
			while (empTable.contains(document.querySelector('.empRow'))) {
        empTable.removeChild(empTable.lastChild);
    	};
			addEmpRow();
		});
	}
};

let url = 'http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2';
fetch(url).then( res => res.json()).then(Construct);

function Construct( info ){
	let data = info;
	console.log(data);
	let table = document.createElement('table');
	table.className = "main table table-bordered";
	table.innerHTML = ` 
		<tr>
		<th>Company</th>
		<th id='balance-head'>Balance</th>
		<th>Registered</th>
		<th>Address</th>
		<th id='employers-head'>Employers</th>
		</tr>
	`;
	document.body.appendChild(table);
	
	data.forEach(item => {
		let record = new Record(
			item.company, 
			item.balance, 
			item.registered, 
			item.address, 
			item.employers
		);
		let ui = new UI();
		ui.addRow(record);
	});

	// Show (address/employers) event
	document.querySelector('.table').addEventListener('click', function(e){
		if (e.target.className === 'addr'){
			let companyName = e.target.parentElement.parentElement.firstElementChild.innerHTML;
			let result = data.find(search =>{ 
				if (search.company === companyName){
					return search
				}
			});

			let ui = new UI();
			ui.showAddress(e.target.parentElement, result.address);
		}
		if (e.target.className === 'emp') {
			let companyName = e.target.parentElement.parentElement.firstElementChild.innerHTML;
			// console.log(companyName);
			let result = data.find(search =>{ 
				if (search.company === companyName){
					return search
				}
			});
			// console.log(result.employers);

			let ui = new UI();
			ui.showEmployers(companyName, result.employers);
		}
		
	});
	
	// Sort by balance
	document.getElementById('balance-head').addEventListener('click', function(e){
		data.sort((a, b) => {
			let first = parseInt(a.balance.split('').slice(1).join(''));
			let second = parseInt(b.balance.split('').slice(1).join(''));
			return first - second;
		});
		data.forEach(item => {
			let record = new Record(item.company, item.balance, item.registered, item.address, item.employers);
			let table = document.querySelector('.table');
			let row = document.querySelector('.item');
			table.removeChild(row);
			let ui = new UI();
			ui.addRow(record);
		});
	});
	
	// Sort by employers
	document.getElementById('employers-head').addEventListener('click', function(e){
		data.sort((a, b) => {
			let first = a.employers.length;
			let second = b.employers.length;
			return first - second;
		});
		data.forEach(item => {
			let record = new Record(item.company, item.balance, item.registered, item.address, item.employers);
			let table = document.querySelector('.table');
			let row = document.querySelector('.item');
			table.removeChild(row);
			let ui = new UI();
			ui.addRow(record);
		});
	});
};