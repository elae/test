/*

1. Создать ф-ю констурктор которая создаст новый обьект вашего типа
2. Обьект должен иметь пару свойств
3. Функцию которая производит манипуляцию со свойствами
4. Функция которая перебором выводит все свойства

*/

function Person(name, age, active){
  this.name = name;
  this.age = age;
  this.isActive = active;
};

Person.prototype.setIsActive = function() {
  this.isActive = !this.isActive;
};

Person.prototype.show = function () {
  console.log(this);
};

let john = new Person("John", 23, true);
let mary = new Person('Mary', 34, false);

john.setIsActive();

john.show();
mary.show();