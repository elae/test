/*
  Задание:

    I. Написать класс Post который будет уметь:

      1. Конструктор:
          title
          image
          description
          likes

      2. Методы
          render -> отрисовать элемент в ленте
          likePost -> увеличивает счетчик постов
          Методы для изменения title, image, description
          + бонус. Сделать получение и изменение через set и get

    II. Написать класс Advertisment который будет экстендится от класа Post
        но помимо будет иметь другой шаблон вывода, а так же метод для покупки обьекта

        buyItem -> выведет сообщение что вы купили обьект

    III.  Сгенерировать ленту из всех постов что вы добавили в систему.
          Каждый третий пост должен быть рекламным
*/

class Post {
    constructor(title, image, description, likes){
        this._title = title;
        this._img = image;
        this._description = description;
        this._likes = likes === undefined ? 0 : likes;
    }

    get title(){
        return this._title.toUpperCase();
    }
    set title(newTitle){
        this._title = `Title: ${newTitle}`;
    }

    render(){
        let post = document.createElement('div');
        post.className = 'card horizontal hoverable';
        post.innerHTML = `
            <div class='card-image'>
            <img src='${this._img}'>
            <h5 class='card-title green'>${this._title}</h5>
            </div>
            <div class='card-content'>
            <p>${this._description}</p>
            </div>
            <div class="card-action valign-wrapper">

            <button class='btn'> ${this._likes}</button>
            </div>
        `;
        document.querySelector('.container').appendChild(post).addEventListener('click', (e) => {
            if(e.target.className === 'btn')
            // console.log(e.target.innerHTML);
            this.likePost(e.target);
        });
    }
    likePost(target){
        target.innerHTML = +target.innerHTML + 1;
    }
}

class Advertisement extends Post{
    constructor(title, image, description, price){
        super(title, image, description);
    }
    render(){
        let post = document.createElement('div');
        post.className = 'buy card horizontal hoverable';
        post.innerHTML = `
            <div class='card-content'>
            <h5>${this._title}</h5>
            <img src='${this._img}'>
            <p>${this._description}</p>
            </div>
            <div class="card-action valign-wrapper">
            <button class='btn-large buy red'>Buy Now!</button>
            </div>
        `;
        document.querySelector('.container').appendChild(post).addEventListener('click', (e) => {
            if(e.target.className === 'btn-large buy red') {
            // console.log(e.target);
            this.buyItem(e.target.parentElement.parentElement);}
        });
    }
    buyItem(target){
        let msg = document.createElement('div');
        msg.className = 'card-panel green';
        msg.innerHTML = `<div class='white-text center-align'><b>Поздравляем! Вы купили объект!</b></div>`;
        let showMsg = target.parentElement.insertBefore(msg, target);
        setTimeout(function(){
            showMsg.remove();
        }, 1000);
        
    }
}

window.onload = function(){
    let text = 'Lorem ipsum dolor sit amet consectetur adipisicing elit Eum commodi molestias soluta sunt officiis laboriosam eos provident magnam nam ab earum nulla necessitatibus excepturi dolor voluptates molestiae quia perspiciatis animi';
    let array = text.split(' ');
    
    array.forEach(item => {
        if((array.indexOf(item)+1) % 3 !== 0) {
            this[item] = new Post(`${item}`, '3dcity.png', `${text}`);
            // console.log(this[item]);
            this[item].render();
        } else {
            this[item] = new Advertisement(`${item}`, '50de8c55af3d1.png', `${text}`);
            this[item].render();
        }
        
    });
}


