/*

  Задание - используя классы и (или) прототипы создать программу, которая будет
  распределять животных по зоопарку.

  Zoo ={
    name: '',
    AnimalCount: 152,
    zones: {
      mammals: [],
      birds: [],
      fishes: [],
      reptile: [],
      others: []
    },
    addAnimal: function(animalObj){
      // Добавляет животное в зоопарк в нужную зону.
      // зона берется из класса который наследует Animal
      // если у животного нету свойства zone - то добавлять в others
    },
    removeAnimal: function('animalName'){
      // удаляет животное из зоопарка
      // поиск по имени
    },
    getAnimal: function(type, value){
      // выводит информацию о животном
      // поиск по имени или типу где type = name/type
      // а value значение выбраного типа поиска
    },
    countAnimals: function(){
      // функция считает кол-во всех животных во всех зонах
      // и выводит в консоль результат
    }
  }

  Есть родительский класс Animal у которого есть методы и свойства:
  Animal {
    name: 'Rex', // имя животного для поиска
    phrase: 'woof!',
    foodType: 'herbivore' | 'carnivore', // Травоядное или Плотоядное животное
    eatSomething: function(){ ... }
  }

  Дальше будут классы, которые расширяют класс Animal - это классы:
  - mammals
  - birds
  - fishes
  - pertile

  каждый из них имеет свои свойства и методы:
  в данном примере уникальными будут run/speed. У птиц будут методы fly & speed и т.д
  Mammals = {
    zone: mamal, // дублирует название зоны, ставиться по умолчанию
    type: 'wolf', // что за животное
    run: function(){
      console.log( wolf Rex run with speed 15 km/h );
    },
    speed: 15
  }

  Тестирование:
    new Zoo('name');
    var Rex = new Mammal('Rex', 'woof', 'herbivore', 15 );
    your_zooName.addAnimal(Rex);
      // Добавит в your_zooName.zones.mamals новое животное.
    var Dex = new Mammal('Dex', 'woof', 'herbivore', 11 );
    your_zooName.addAnimal(Dex);
      // Добавит в your_zooName.zones.mamals еще одно новое животное.

    your_zooName.get('name', 'Rex'); -> {name:"Rex", type: 'wolf' ...}
    your_zooName.get('type', 'wolf'); -> [{RexObj},{DexObj}];

    Программу можно расширить и сделать в виде мини игры с интерфейсом и сдать
    как курсовую работу!
    Идеи:
    - Добавить ранжирование на травоядных и хищников
    - добавив какую-то функцию которая иммитирует жизнь в зоопарке. Питание, движение, сон животных и т.д
    - Условия: Если хищник и травоядный попадает в одну зону, то хищник сьедает травоядное и оно удаляется из зоопарка.
    - Графическая оболочка под программу.
*/

class Zoo {
  constructor(name) {
    this.name = name;
    this.animalCount = 0;
    this.zones = {
      mammals: [],
      birds: [],
      fishes: [],
      reptiles: [],
      others: []
    };
  }
  addAnimal(animal){
    switch(animal.zone){
      case 'mammals':
        console.log(`${animal.name} added to mammals`);
        this.zones.mammals.push(animal);
        break;
      case 'birds':
        console.log(`${animal.name} added to birds`);
        this.zones.birds.push(animal);
        break;
      case 'fishes':
        console.log(`${animal.name} added to fishes`);
        this.zones.fishes.push(animal);
        break;
      case 'reptiles':
        console.log(`${animal.name} added to reptiles`);
        this.zones.reptiles.push(animal);
        break;
      default:
        console.log(`${animal.name} added to others`);
        this.zones.others.push(animal);
    }
  }
  removeAnimal(animalName){
    let zoneList = [this.zones.mammals, this.zones.birds, this.zones.fishes, this.zones.reptiles, this.zones.others];
    zoneList.forEach(zone => {
      zone.forEach(animal => {
        if(animal.name === animalName) {
          zone.splice(zone.indexOf(animal), 1);
          console.log(`${animal.type} ${animal.name} removed`);
        }
      });  
    });
  }
  getAnimal(type, value){
      // выводит информацию о животном
      // поиск по имени или типу где type = name/type
      // а value значение выбраного типа поиска
    let zoneList = [this.zones.mammals, this.zones.birds, this.zones.fishes, this.zones.reptiles, this.zones.others];
    let res = [];
    if(type === 'name'){
      zoneList.forEach(zone => {
        zone.forEach(animal => {
          if(animal.name === value) {res = animal}
        });
      });
    } else if (type === 'type') {
        zoneList.forEach(zone => {
          zone.forEach(animal => {
            if(animal.type === value) {res.push(animal)}
          });
        })
      };
    console.log(res);
  }
  countAnimals(){
    let zoneList = [this.zones.mammals, this.zones.birds, this.zones.fishes, this.zones.reptiles, this.zones.others];
    let result = 0;
    zoneList.forEach(zone => {result += zone.length});
    console.log(`total: ${result} animals`);
  }
};

class Animal {
  constructor(name, phrase, foodType){
    this.name = name;
    this.phrase = phrase;
    this.foodType = foodType;
  }
  eatSomething(){
    console.log(`${this.name} is eating`);
  }
};

class Mammal extends Animal {
  constructor(name, phrase='woof', foodType='herbivore', type='wolf', speed = 15) {
    super(name, phrase, foodType);
    this.zone = 'mammals';
    this.type = type;
    this.speed = speed;
  }
  run(){
    console.log(`${this.type} ${this.name} runs with speed ${this.speed} km/h`);
  }
};

class Bird extends Animal {
  constructor(name, phrase='twee-twee', foodType='carnivore', type='parrot', speed = 10) {
    super(name, phrase, foodType);
    this.zone = 'birds';
    this.type = type;
    this.speed = speed;
  }
  fly(){
    console.log(`${this.type} ${this.name} flies with speed ${this.speed} km/h`);
  }
};

class Fish extends Animal {
  constructor(name, phrase='...', foodType='herbivore', type='fish', speed = 5) {
    super(name, phrase, foodType);
    this.zone = 'fishes';
    this.type = type;
    this.speed = speed;
  }
  swim(){
    console.log(`${this.type} ${this.name} swims with speed ${this.speed} km/h`);
  }
};

class Reptile extends Animal {
  constructor(name, phrase='...', foodType='carnivore', type='crocodile', speed = 5) {
    super(name, phrase, foodType);
    this.zone = 'reptiles';
    this.type = type;
    this.speed = speed;
  }
  crawl(){
    console.log(`${this.type} ${this.name} crawls with speed ${this.speed} km/h`);
  }
};

let happy = new Zoo('Happy');
let rex = new Mammal('Rex', 'woof', 'carnivore', 'wolf');
let sparky = new Bird('Sparky');


happy.addAnimal(rex);
happy.addAnimal(sparky);
rex.run();
// happy.removeAnimal('Rex');
happy.getAnimal('type', 'wolf');
happy.countAnimals();