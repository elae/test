let obj = [
  {
    link: "#1",
    name: "Established fact",
    description: "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. ",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg"
  },
  {
    link: "#2",
    name: "Many packages",
    description: "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400859_file_162309.jpg"
  },
  {
    link: "#3",
    name: "Suffered alteration",
    description: "Looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature.",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400896_file_162315.jpg"
  },{
    link: "#4",
    name: "Discovered source",
    description: "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400878_file_162324.jpg"
  },{
    link: "#5",
    name: "Handful model",
    description: "The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400876_file_162328.jpg"
  },
];


function func() {
  obj.forEach(function(element){

   let div = document.createElement('div');
   let link = document.createElement('a');
   let name = document.createElement('h1');
   let desc = document.createElement('p');
   let image = document.createElement('img');
   let count = 0;
   let btn = document.createElement('button');
 
   link.setAttribute('href', element.link);
   name.innerHTML = element.name;
   desc.innerHTML = element.description;
   image.setAttribute('src', element.image);
   btn.innerText = count;

   div.appendChild(image);
   div.appendChild(name);
   div.appendChild(desc);
  //  div.appendChild(link);
   div.appendChild(btn);

   document.body.appendChild(div);
   
   btn.addEventListener('click', function(){
    btn.innerText = ++count;
   });

  });
};

func();