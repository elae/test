/*
  Задание:

  Написать фабрику, которая создает планеты.
    Есть публичные свойства и методы:
      name: "",
      population: randomPopulation(),
      timeCycle: 24,

      growPopulation: () => {
        функция которая берет приватное свойство populationMultiplyRate
        которое равняется случайному числу от 1 до 10 и умножает его на 1/1000 от популяции.
        Дальше, число которое вышло добавляет к популяции и выводит в консоль сообщение,
        что за один цикл прибавилось столько населения планеты.
      }
      rotatePlanet: () => {
        функция которая на основе зарандомленого (Math.round) числа делает действие.
        вначале рандомим случайное число. Если число делится на 2 без остачи - запускаем метод
        growPopulation. Если без остачи не делиться - то запускаем приватный метод Cataclysm
      }

    Приватные свойства и методы:
      randomPopulation: () => Возвращает случайное целое число от 1.000.000 до 100.000.000
      populationMultiplyRate - случайное число от 1 до 10
      Cataclysm: () => {
        Рандомим число от 1 до 10 и умножаем его на 10000;
        То число которое получили, отнимаем от популяции.
        В консоль выводим сообщение - от катаклизма погибло Х человек.
      }
*/

const Planet = (name) => {
  const populationMultiplyRate = Math.floor(Math.random() * 10) + 1;
  const randomPopulation = () => {
    return Math.floor(Math.random() * (100000000 - 1000000 + 1)) + 1000000;
  }
  const Cataclysm = () => {
    let x = (Math.floor(Math.random() * 10) + 1) * 10000;
    this.population -= x;
    console.log(`От катаклизма погибло ${x} человек`);
  }

  let obj = Object.assign(
    {},
    {
      name: name,
      timeCycle: 24,
      population: randomPopulation(),
      growPopulation: () => {
        let y = populationMultiplyRate * Math.floor(obj.population / 1000);
        obj.population += y;
        console.log(`за один цикл на планете ${obj.name} прибавилось ${y} человек`);
      },
      rotatePlanet: () => {
        if(Math.round(Math.random()) % 2) {
          obj.growPopulation();
        } else { Cataclysm() };
      }
    }
  );
  return obj;
}

const earth = Planet('Earth');

console.log(earth);

earth.growPopulation();

console.log(earth);

for(let i = 0; i<=20; i++){
  earth.rotatePlanet();
}

console.log(earth);