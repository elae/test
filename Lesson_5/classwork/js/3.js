const swim = (state) => ({
  swim: () => console.log(state.name, 'is swiming')
});

export default swim;