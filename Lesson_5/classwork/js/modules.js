import run from "./1.js";
import eat from './2.js';
import swim from './3.js';

const Pinguin = (name) => {
  let state = {
    name,
    type: 'pinguin'
  };
  return Object.assign(
    {},
    state,
    eat(state),
    swim(state)
  );
};

const Ostrich = (name) => {
  let state = {
    name,
    type: 'ostrich'
  };
  return Object.assign(
    {},
    state,
    eat(state),
    run(state)
  );
};

const bismark = Pinguin('Bismark');
bismark.eat();
bismark.swim();

const john = Ostrich('John');
john.eat();
john.run();