const eat = (state) => ({
  eat: () => console.log(state.name, 'is eating')
});

export default eat;