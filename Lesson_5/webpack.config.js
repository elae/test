const path = require('path');
module.exports = {
  entry: './classwork/js/modules.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'classwork')
  },
  devServer: {
    contentBase: './classwork',
    watchContentBase: true
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        options: {
          presets: ['env']
        }
      }
    ]
  }
};
