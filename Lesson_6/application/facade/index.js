import api from './api';

const FacadeDemo = () => {

  /*
    Фасад — это структурный паттерн проектирования,
    который предоставляет простой интерфейс
    к сложной системе классов, библиотеке или фреймворку.

    https://refactoring.guru/ru/design-patterns/facade
  */
  const myFunc = ( data ) => {
    console.log( 'myFunc', data );
    api.getList();
    console.log(
       'fetch status', api.getFetchStatus(),
       'user', api.getUser(0)
      );
  };

  api.fetchData().then( myFunc );
};

export default FacadeDemo;
