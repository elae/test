// Точка входа в наше приложение

import Singleton from './singleton/';
import Facade from './facade';

import Work1 from '../classworks/objectfreeze.js';
import government from '../classworks/singleton.js';

/*
  Обратите внимание что в среде разработки Singleton / singleton
  это один файл и ошибки из-за реестра не будет, но в продакшене
  это может дать ошибку, потому что например на CentOS такие импотры
  уже не отрабатывают и еррорят.

  + в файлах можно не указывать .js
  + можно подключать файлы с названием index.js обращаясь напрямую к папке
  напрмер если нужно подключить файл ./singleton/index.js,
  то можно зададь в url просто ./singleton
*/

/*
  Партерны можно поделить на три большие группы:
  - Порождающие
  - Структурные
  - Поведенческие

*/

  // Singleton();
  // Facade();
  // Work1();

  console.log();
  government.addLaw(1, 'first', 'bla-bla-bla');
  government.addLaw(2, 'second', 'fkvnjfklvndfklvn');
  government.readConstitution();
  government.readLaw(1);
  government.satisfactionLevel();
  government.budget();
  government.celebrate();
  government.satisfactionLevel();