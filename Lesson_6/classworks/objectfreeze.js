/*
  Задание: написать функцию, для глубокой заморозки обьектов.

  Обьект для работы:
  let universe = {
    infinity: Infinity,
    good: ['cats', 'love', 'rock-n-roll'],
    evil: {
      bonuses: ['cookies', 'great look']
    }
  };

  frozenUniverse.evil.humans = []; -> Не должен отработать.

  Методы для работы:
  1. Object.getOwnPropertyNames(obj);
      -> Получаем имена свойств из объекта obj в виде массива

  2. Проверка через typeof на обьект или !== null
  if (typeof prop == 'object' && prop !== null){...}

  Тестирование:

  let FarGalaxy = DeepFreeze(universe);
      FarGalaxy.good.push('javascript'); // true
      FarGalaxy.something = 'Wow!'; // false
      FarGalaxy.evil.humans = [];   // false

*/

let univers = {
  infinity: Infinity,
  good: ['cats', 'love', 'rock-n-roll'],
  evil: {
    bonuses: ['cookies', 'great look']
  }
};


const doSmsng = () => 22;
const Work1 = () => {


let DeepFreeze = (obj) => {
  console.log(obj);
  

  console.log(Object.isFrozen( obj ));
  
  let properties = Object.getOwnPropertyNames(obj);
  properties.forEach(prop => {
    console.log(prop);
    if(typeof obj[prop] === 'object' && obj[prop] !== null) {
    DeepFreeze(obj[prop]);
    console.log(Object.isFrozen( prop ));
    }
  });
  return Object.freeze(obj);
}

let FarGalaxy = DeepFreeze(univers);
      FarGalaxy.good.push('javascript'); // true
      FarGalaxy.something = 'Wow!'; // false
      FarGalaxy.evil.humans = [];   // false

      console.log( FarGalaxy, Object.isFrozen( FarGalaxy.good ) );
}

export default Work1;