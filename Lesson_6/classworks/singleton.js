/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
            
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5


*/


const data = {
  laws: [],
  budget: 1000000,
  citizensSatisfactions: 0
};

const government = {
  addLaw: (id, name, description) => {
    console.log(id, name, description);
    data.laws.push({id: id, name: name, description: description});
    data.citizensSatisfactions -= 10;
  },
  readConstitution: () => {
    console.log(data.laws);
  },
  readLaw: (id) => {
    data.laws.forEach((law) => {
      if(law.id === id) console.log(law.description)
    });
  },
  satisfactionLevel: () => {
    console.log(data.citizensSatisfactions);
  },
  budget: () => {
    console.log(data.budget);
  },
  celebrate: () => {
    data.budget -= 50000;
    data.citizensSatisfactions += 5;
  }
};

Object.freeze(government);

export default government;